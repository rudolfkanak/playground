import React, {Component, PropTypes} from 'react';
import Comment from '../components/CommentsWidget/Comment';
import NavigationComponent from  '../components/NavigationComponents/NavigationComponent';
import SchedulerComponent from  '../components/SchedulerComponent/SchedulerComponent';

export default class App extends Component {
    render() {
        const comments = [];
        const styles = require('./App.scss');
        return (
            <div id="main-container">
                <NavigationComponent />
                <div className={styles.container}>
                    <h1>Comments</h1>
                    <Comment comments={comments} />
                    <SchedulerComponent />
                </div>
            </div>
        );
    }
}
