import React, {Component} from 'react';
import { Table } from 'react-bootstrap';

const SchedulerHead = (props) => {
    const { columns } = props;

    var htmlColumns = [];
    columns.map(function(col, index) {
        htmlColumns.push(<th>{col}</th>);
    });

    return (
        <thead>
        <tr>
            { htmlColumns }
        </tr>
        </thead>
    );
};

const SchedulerRows = (props) => {
    const { headerCountColumns, maxRows } = props;
    var htmlRows = [];

    for(let i=0; i < maxRows; i++) {
        var row = '<tr>';
        for(let x=0; x < headerCountColumns; x++) {
            row += '<td>&nbsp;</td>';
        }
        row += '</tr>'

        htmlRows.push(row);
    }

    return (
        <thead>
            {htmlRows}
        </thead>
    );
};

class SchedulerComponent extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            headerColumns: ['Pondělí 01.05.2016', 'Úterý 02.05.2016', 'Středa 03.05.2016', 'Čtvrtek 04.05.2016', 'Pátek 05.05.2016', 'Sobota 06.05.2016', 'Neděle 07.05.2016'],
            maxRows: 60, // 5 min
            schedulerData: [{
                headerColumn: 1,
                bodyRows: {
                    from: 1,
                    to: 9
                }
            }]
        }
    }

    render() {
        return (
            <Table striped bordered condensed hover>
                <SchedulerHead key="0" columns = {this.state.headerColumns} />
                <SchedulerRows key="1" headerColumnCount = {this.state.headerColumns.length} maxRows = {this.state.maxRows} />
            </Table>
        );
    }
}

export default SchedulerComponent;


