import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import CommentAdd from './CommentAdd';
import CommentList from './CommentList';
import { connect } from 'react-redux';
import * as commentsActions from '../../redux/modules/comments';
import { Button } from 'react-bootstrap';
import $ from 'jquery';

class Comment extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      data: []
    };
  }

  handleCommentSubmit(postText) {
    let {addComment} = this.props;
    addComment({name: 'Rudolf Kanak', text: postText});
  }

  handleRemoveComment (e, itemId) {
    let { removeComment } = this.props;
    
    removeComment(itemId)
  }

  componentDidMount () {
    let { reloadComments } = this.props;

    setTimeout(function() {
      reloadComments([{id: 0, name: 'Rudolf Kanak', text: 'hello world'}]);
    }.bind(this), 1000);
  }

  render() {
    const { comments, actions } = this.props;
    const salute = require('../../lib/salute.js');
    
    return (

      <div className="container-form">
        {salute()}
        <Button ref="btn" bsStyle="danger">Warning</Button>

        <CommentList comments={this.state.data} onClick={ (e, itemId) => this.handleRemoveComment(e, itemId) } />
        <CommentAdd onSubmit={(e) => this.handleCommentSubmit(e)} />
      </div>
    );
  }

  componentDidMount() {
    console.log(this.refs.btn)
    var myComponent = ReactDOM.findDOMNode(this.refs.btn);
    $(myComponent).css("background","blue");
  }
}

export default connect(state => state, commentsActions)(Comment)


