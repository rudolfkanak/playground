import React, {Component} from 'react';

export default class CommentItem extends Component {
	
	constructor (props, context) {
        super(props, context);
    }

    handleClick (e, id) {        
    	this.props.onClick(e, id);
    }

    render() {
    	let { item } = this.props;

        return (
            <div className="comment-item">
                <strong>Author:</strong> { item.name } - <a href="#" onClick={ (e) => this.handleClick(e, item.id) }>[X]</a> <br />
                {item.text}
                <br />
                <hr />
            </div>
        );
    }
}