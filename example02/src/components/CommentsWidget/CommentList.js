import React, {Component} from 'react';
import { connect } from 'react-redux';
import CommentItem from './CommentItem';

export default class CommentList extends Component {

    constructor (props, context) {
        super(props, context);
        this.state = {comments: []};
    }

    render() {
        const { comments } = this.props;
        var rows = [];
        
        comments.map(function(comment, index) {
           rows.push(<CommentItem key={index} item={comment} onClick={this.props.onClick} />);
        }.bind(this));        
        
        return (
            <div className="comment-list">
                { rows }
            </div>
        );
    }
}

function mapStateToProps(state) {
    return { comments: state.comments };
}

export default connect(mapStateToProps)(CommentList)
