// zruseni mokovani funkce aby jest nedaval facke fn
jest.unmock('../lib/sum'); // unmock to use the actual implementation of sum

describe('sum', () => {
    it('adds 1 + 2 to equal 3', () => {
        function f (x, y, ...params) {
            return (x + y) * params.length;
        }

        console.log(f(1, 2, 3, 4));
        const name = 'Ruda';
        const lastName = "Kanak";

        var a,b;
        var list = [1, 2, 3]
        var [a, , b] = list;

        [b, a] = [a, b];

        console.log(a);
        console.log(b);

        class Person {


            constructor(firstName, lastName) {
                this._firstName = firstName;
                this._lastName = lastName;
            }

            fullName() {
                return `Person name is ${this._firstName} ${this._lastName}`;
            }

            get nickName()  { return this._firstName };
        }

        let me = new Person('Rudolf', 'Kanak');
        console.log(me.fullName());
        console.log(me.nickName);

        function* idMarker() {
            var index = 0;
            while(true) {
                yield index++;
            }
        }
        var gen = idMarker();

        console.log(gen.next().value);
        console.log(gen.next().value);
        console.log(gen.next().value);

        var dst = { quux: 0 };
        var src1 = { foo: 1, bar: 2 };
        var src2 = { foo: 3, bar: 4 };
        Object.assign(dst, src1, src2);
        console.log(dst);

        let findValue = [ 1, 3, 4, 2 ].find(x => x > 3);
        console.log(findValue);

        // Promise
        function msgAfterTimeout (msg, who, timeout) {
            return new Promise((resolve, reject) => {
                setTimeout(() => resolve(`${msg} Hello ${who}!`), timeout)
            })
        }
        msgAfterTimeout("", "Foo", 100).then((msg) =>
            msgAfterTimeout(msg, "Bar", 200)
        ).then((msg) => {
            console.log(`done after 300ms:${msg}`)
        })
    });
});