import { combineReducers } from 'redux'
import comments from './modules/comments'

const rootReducer = combineReducers({
  comments
})

export default rootReducer