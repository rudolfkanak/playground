import React, {Component, PropTypes} from 'react';
import ShoppingComponent from '../../components/Shopping/ShoppingComponent'

export default class App extends Component {
    render() {
        return (
            <div className="container theme-showcase">
                <ShoppingComponent />
            </div>);
    }
}
