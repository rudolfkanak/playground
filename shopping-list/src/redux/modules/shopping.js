const ADD_ITEM = 'ADD_ITEM';
const REMOVE_ITEM = 'REMOVE_ITEM';
const LOAD_ITEMS = 'LOAD_ITEMS';
const UPDATE_ITEM = 'UPDATE_ITEM';
const FILTER_STATE_APPLY = 'FILTER_STATE_APPLY';


const initialState = {
    items: [],
    isUpdate: false,
    filterStateBy: null
};

export default function shopping(state = initialState, action) {
    switch (action.type) {
        case ADD_ITEM:
            const data = [
                ...state.items,
                {
                    id: state.items.reduce((maxId, item) => Math.max(item.id, maxId), -1) + 1,
                    name: action.item.name,
                    count: action.item.count,
                    state: itemState.ACTIVE
                }
            ];
            return {items: data, isUpdate: true, filterStateBy: null};
        case REMOVE_ITEM:
            const newData = state.items.filter(i => i.id != action.id);
            return {items: newData, isUpdate: true, filterStateBy: state.filterStateBy};
        case UPDATE_ITEM:
            const { item } = action;
            const updateData = state.items.map((i) => {
                if (i.id === item.id)
                    return item;

                return i;
            });
            return {items: updateData, isUpdate: true, filterStateBy: state.filterStateBy};
        case FILTER_STATE_APPLY:
            return {items: state.items, filterStateBy: action.state, isUpdate: false};
        case LOAD_ITEMS:            
            return {items: action.items, isUpdate: true, filterStateBy: null, isUpdate: false};;
        default:
            return state
    }
}

export const addItemAction = (item) => {
    return { type: ADD_ITEM, item };
};

export const removeItemAction = (id) => {
    return { type: REMOVE_ITEM, id };
};

export const loadItemsAction = (items) => {
    return { type: LOAD_ITEMS, items };
};

export const updateItemAction = (item) => {
    return { type: UPDATE_ITEM, item };
};

export const itemFilterByStateAction = (state) => {
    return { type: FILTER_STATE_APPLY, state };
};

export const itemState = Object.assign ({
    ALL: 'all',
    ACTIVE: 'active',
    COMPLETED: 'completed'
});
