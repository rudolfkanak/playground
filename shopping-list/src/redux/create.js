import { combineReducers } from 'redux'
import shopping from './modules/shopping'

const rootReducer = combineReducers({
  shopping
})

export default rootReducer