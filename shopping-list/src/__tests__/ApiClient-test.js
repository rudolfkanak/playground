jest.unmock('../lib/ApiClient');
jest.unmock('jquery');

import timekeeper from 'timekeeper';
import ApiClient from  '../lib/ApiClient';

describe('apiClient', () => {

    pit('Asynch test save value', () => {
        const apiClient = new ApiClient();

        const actualProduct = {id: 0, category: 'Pečivo', name: 'Chleba bílý', 'Price': 10};
        const testParameter = 'test_parameter';

        return apiClient.save(testParameter, actualProduct).then((data) => {
            expect(data.id).toBe(222);
        });
    })
});

