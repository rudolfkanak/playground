import 'babel-polyfill';
import React from 'react';
import {render} from 'react-dom';
import { Provider } from 'react-redux';
import App from './containers/App/App';
import configureStore from '../store/configureStore';
import ApiClient from './lib/ApiClient';
import ShoppingRepository from './lib/repositories/shoppingRepository';

const store = configureStore();

render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('main-wrapper')
);

store.subscribe(() => {
    const { items, isUpdate } = store.getState().shopping;

    if (isUpdate) {
        const shoppingRepository = new ShoppingRepository(new ApiClient());
        shoppingRepository.adds(items).then((data) => console.info('save...'));
    }
});
