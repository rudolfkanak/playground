module.exports = Object.assign({
    api: {
        url: {
            tokenGenerateUrl: 'https://uuos9.plus4u.net/uu%3A0%3AARTIFACT%3FUUproto%3AOS%2FService%2FLogin%3AcallTokenGenerate',
            saveUrl: 'https://uuos9.plus4u.net/uu%3ADEV0135-BT%3AARTIFACT%3FUU%3ASTORAGE%3AStorage%2Fsave',
            loadUrl: 'https://uuos9.plus4u.net/uu%3ADEV0135-BT%3AARTIFACT%3FUU%3ASTORAGE%3AStorage%2Fload',
            deleteUrl: 'https://uuos9.plus4u.net/uu%3ADEV0135-BT%3AARTIFACT%3FUU%3ASTORAGE%3AStorage%2Fdelete'
        },
        header: {
            accept: 'application/json',
            UU_EAC: '6861666861663a6e61666e6166'
        },
        tokenRequestBody: '{"data":{"outputTo":"data"}}',
        tokenRequestPostBody: '{"data":{"dataKey":"RK_DATA"}}',
        method: 'POST',
        dataStorage: 'SERVER', //LOCAL OR SERVER,
        dataKey: "RK_DATA",
        tokenExpireTime: (30 * 60 * 1000) // 30min
    }
});
