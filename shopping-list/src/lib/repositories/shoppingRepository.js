import jQuery from 'jquery';

export default class ShoppingRepository {

    constructor(apiClient) {
        this._apiClient = apiClient;
        this._apiKey = 'SHOPPING';
    }

    adds(items) {
        let dfd = jQuery.Deferred();
        console.info('Call method adds');

        this._apiClient.save(this._apiKey, items).then(() => dfd.resolve());

        return dfd;
    }

    delete(items) {
        const dfd = jQuery.Deferred();
        console.info('Call method delete');

        this._apiClient.save(this._apiKey, items).then(() => dfd.resolve());

        return dfd;
    }

    getAll() {
        let dfd = jQuery.Deferred();
        console.info('Call get all items.');

        this._apiClient.get(this._apiKey).then((data) => {
            if (data === null) {
                return dfd.resolve([]);
            } else {
                return dfd.resolve(data);
            }

        });

        return dfd;
    }
};

