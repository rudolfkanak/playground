import config from '../config';
import jQuery from 'jquery';
import _ from 'lodash';

class ApiClient {

    constructor() {
        this.apiConfig = config.api;
        this.validToken = null;
    }

    // Public methods
    save(parameterName, dataContent) {
        const dfd = jQuery.Deferred();

        if (this._isTokenValid()) {
            console.info('Save data with parameter name "' + parameterName + '"  and values "' + JSON.stringify(dataContent) + '"');
            return this._pushRequest(this.apiConfig.url.saveUrl, false, this._buildSaveWrapperRequest(parameterName, dataContent));
        } else {
            this._requestToken().then((data) => {
                this._pushRequest(this.apiConfig.url.saveUrl, false, this._buildSaveWrapperRequest(parameterName, dataContent)).then((value) => {
                    console.info('Save data with parameter name "' + parameterName + '" and values "' + JSON.stringify(dataContent) + '"');
                    dfd.resolve(value);
                });
            });
        }

        return dfd;
    }

    delete(parameterName, dataContent) {
        return this.save(parameterName, dataContent);
    }

    get(parameterName) {
        let dfd = jQuery.Deferred();

        if (this._isTokenValid()) {
            this._pushRequest(this.apiConfig.url.loadUrl, false, this.apiConfig.tokenRequestPostBody)
                .then((value) => dfd.resolve(this._getDataByParameterName(value, parameterName)));
        } else {
            this._requestToken().then(() => {
                this._pushRequest(this.apiConfig.url.loadUrl, false, this.apiConfig.tokenRequestPostBody)
                    .then((value) => dfd.resolve(this._getDataByParameterName(value, parameterName)));
            });
        }

        return dfd;
    }

    clearAllData() {

        let dfd = jQuery.Deferred();

        if (this._isTokenValid()) {
            return this._pushRequest(this.apiConfig.url.deleteUrl, false, this.apiConfig.tokenRequestPostBody);
        } else {
            this._requestToken().then(() => {
                this._pushRequest(this.apiConfig.url.deleteUrl, false, this.apiConfig.tokenRequestPostBody)
                    .then((value) => dfd.resolve(value));
            });
        }

        return dfd;
    }

    // Private methods
    _requestToken() {
        let dfd = jQuery.Deferred();

        this._pushRequest(
            this.apiConfig.url.tokenGenerateUrl,
            true,
            this.apiConfig.tokenRequestBody).then((data) => {
                this.validToken = {
                    timestamp: new Date(new Date().getTime() + this.apiConfig.tokenExpireTime).getTime(),
                    token: JSON.parse(data).data.UU_CT
                };
                this._saveTokenInfo(this.validToken);
                dfd.resolve();
        });

        return dfd;
    };

    _pushRequest(url, isToken, content) {
        let dfd = jQuery.Deferred();

        jQuery.ajax(url, {
            type: this.apiConfig.method,
            headers: isToken ? { "Accept": this.apiConfig.header.accept, "UU_EAC": this.apiConfig.header.UU_EAC }
                : { "Accept": this.apiConfig.header.accept, "UU_CT": this.validToken.token },
            data: content
        }).done(function (value) {
            dfd.resolve(value);
        }).error(function () {
            throw new Error("Server error.");
        });

        return dfd;
    };

    _saveTokenInfo(validToken) {
        localStorage.setItem('validTokenTimestamp', validToken.timestamp);
    };

    _isTokenValid() {
        return (this.validToken !== null && parseInt(localStorage.getItem('validTokenTimestamp')) >= new Date().getTime());
    };

    _buildSaveWrapperRequest(parameterName, dataContent) {
        let saveData = {};
        saveData["dataKey"] = this.apiConfig.dataKey;
        saveData[parameterName] = dataContent;

        return JSON.stringify({"data": saveData});
    };

    _getDataByParameterName(data, parameterName) {
        let dataAsObj = JSON.parse(data);

        if (!_.isUndefined(dataAsObj.data[parameterName])) {
            return dataAsObj.data[parameterName];
        } else {
            return null;
        }

        //throw new Error("Parameter name '" + parameterName + "' not found.");
    };
}

module.exports = ApiClient;
