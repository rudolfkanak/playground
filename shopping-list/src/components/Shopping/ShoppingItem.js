import React, { PropTypes } from 'react';
import EditItemForm from './AddShoppingComponent';

const ShoppingItem = ({ id, name, count, state, onRemoveClick, onChangeCompletedState }) => {
    const styles = require('./ShoppingItem.scss');
    return(
    <li className={ (state === 'completed' ? styles.completed:styles.active) }>
        <div className="view">
            <input type="checkbox" checked = { state === 'completed' ? 'checked' : '' } className="toggle" onChange={ onChangeCompletedState } />
            <label onClick={ (e) => { isEdit = true } }>{name} </label> <label> {count} </label>
            <i title="Remove item" className={' fa fa-trash-o fa-4'} aria-hidden="true" onClick={ onRemoveClick }></i>
        </div>
        <EditItemForm isEditMode="true" id={id} name={name} count={count} state={state} />
    </li>
)};

ShoppingItem.propTypes = {
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    count: PropTypes.string.isRequired,
    state: PropTypes.string.isRequired,
    onRemoveClick: PropTypes.func.isRequired,
    onChangeCompletedState: PropTypes.func.isRequired
};

export default ShoppingItem
