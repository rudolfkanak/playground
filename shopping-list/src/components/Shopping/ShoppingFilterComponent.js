import React from 'react';
import { connect } from 'react-redux';
import { itemState, itemFilterByStateAction } from '../../redux/modules/shopping';
import styles from './ShoppingFilterComponent.scss';

const ShoppingFilterComponent = ({ items, onAllFilterClick, onActiveFilterClick, onCompletedFilterClick }) => (
    <ul className={styles.filterListGroup}>
        <li>{ items.length } items</li>
        <li><a className="selected" href="#" onClick={ onAllFilterClick } title="Display all items">All</a></li>
        <li><a href="#" onClick={ onActiveFilterClick } title="Display only Active">Active</a></li>
        <li><a href="#" onClick={ onCompletedFilterClick } title="Display only Completed">Completed</a></li>
    </ul>
);

function mapStateToProps(state) {
    return { items: state.shopping.items };
}

const mapDispatchToProps = (dispatch) => {
    return {
        onAllFilterClick: () => {
            dispatch(itemFilterByStateAction(null));
        },

        onActiveFilterClick: () => {
            dispatch(itemFilterByStateAction(itemState.ACTIVE));
        },

        onCompletedFilterClick: () => {
            dispatch(itemFilterByStateAction(itemState.COMPLETED));
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ShoppingFilterComponent)
