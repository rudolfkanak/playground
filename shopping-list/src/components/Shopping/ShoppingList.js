import React from 'react';
import { connect } from 'react-redux';
import Item from './ShoppingItem';
import { removeItemAction, updateItemAction, itemState } from '../../redux/modules/shopping';
import styles from './ShoppingListComponent.scss';

const ShoppingList = ({ items, onItemRemoveClick, onItemChangeStateClick, onUpdateItem, onShowEdit }) => (
    <ul className={styles.shoppingListGroup}>
        {
            items.map(
                item => <Item key={item.id}
                              {...item}
                              onRemoveClick={() => onItemRemoveClick(item.id)}
                              onChangeCompletedState={ () => onItemChangeStateClick(item)}
                              onUpdateItem = {onUpdateItem} /> )
        }
    </ul>
);

function mapStateToProps(state) {
    let filterItems = state.shopping.items;

    if (state.shopping.filterStateBy !== null) {
        filterItems = state.shopping.items.filter(i => i.state === state.shopping.filterStateBy);
    }

    return { items: filterItems };
}

const mapDispatchToProps = (dispatch) => {
    return {
        onItemRemoveClick: (id) => {
            dispatch(removeItemAction(id));
        },
        onItemChangeStateClick: (item) => {
            item.state = (item.state === itemState.ACTIVE ? itemState.COMPLETED : itemState.ACTIVE);
            dispatch(updateItemAction(item));
        },
        onUpdateItem: (name, count) => {
            dispatch(updateItemAction(item));
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ShoppingList)
