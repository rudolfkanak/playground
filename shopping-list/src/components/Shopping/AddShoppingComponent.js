import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import { addItemAction, updateItemAction } from '../../redux/modules/shopping'

let AddShoppingComponent = ({ dispatch, isEditMode = false, id = 0, name = '', count = 1, state = '' }) => {
    let txtInput, countInput;

    return (
        <div>
            <form className="form-horizontal" role="form"
                onSubmit={e => {
                e.preventDefault();

                if (!txtInput.value.trim()) {
                    return
                }

                if (isEditMode) {
                    dispatch(updateItemAction({id: id, name: txtInput.value, count: countInput.value, state: state}));
                } else {
                    dispatch(addItemAction({id: id, name: txtInput.value, count: countInput.value}))
                    txtInput.value = ''
                }
            }}>
                <div className="form-group row">
                    <div className="col-md-7">
                        <input type="text" className="form-control" id="name" ref={node => { txtInput = node }} defaultValue={name} placeholder="Item name..." />
                    </div>
                    <div className="col-md-1">
                        <input type="number" className="form-control" id="count" ref={node => { countInput = node }} defaultValue={count} min="1" />
                    </div>
                    <div className="col-md-3">
                        <button type="submit" className="btn btn-default">{ isEditMode ? 'Edit': 'Add' }</button>
                    </div>
                </div>
            </form>
        </div>
    )
}


AddShoppingComponent = connect()(AddShoppingComponent);
export default AddShoppingComponent;
