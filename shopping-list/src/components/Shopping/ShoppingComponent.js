import React, {Component} from 'react';
import { connect } from 'react-redux';
import AddItem from './AddShoppingComponent';
import List from './ShoppingList';
import FilterItem from './ShoppingFilterComponent';
import * as shoppingActions from '../../redux/modules/shopping';
import ApiClient from '../../lib/ApiClient';
import ShoppingRepository from '../../lib/repositories/shoppingRepository';

class ShoppingComponent extends Component {

    constructor(props, context) {
        super(props, context);
    }

    render() {
        const styles = require('./ShoppingComponent.scss');
        return (
            <section>
                <div className={styles.shoppingWrapper}>
                    <div className="row">
                        <div className="col-md-12 col-12 col-md-offset-1">
                            <header>
                                <h1>Nákupní seznam</h1>
                                <AddItem />
                            </header>
                            <section>
                                <List />
                            </section>
                            <footer>
                                <FilterItem />
                            </footer>
                        </div>
                    </div>
                </div>
            </section>
        );
    }

    componentDidMount() {
        const { loadItemsAction, adds } = this.props;
        const shoppingRepository = new ShoppingRepository(new ApiClient());

        shoppingRepository.getAll().then((items) => loadItemsAction(items));
    }
}

export default connect(state => state, shoppingActions)(ShoppingComponent)
