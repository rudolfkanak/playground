import { createStore } from 'redux'
import rootReducer from '../src/redux/create'

function configureStore(initialState) {
  return createStore(rootReducer, initialState);
}

module.exports = configureStore;