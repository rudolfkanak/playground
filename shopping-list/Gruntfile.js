module.exports = function (grunt) {
    require("matchdep").filterAll("grunt-*").forEach(grunt.loadNpmTasks);

    var path = require('path');
    var webpack = require("webpack");
    var webpackConfig = require("./webpack.config.js");
    var webpackProdConfig = require("./webpack.config.production.js");

    grunt.initConfig({
        webpack: {
            options: webpackProdConfig,
            build: {
                plugins: webpackProdConfig.plugins.concat(
                    new webpack.DefinePlugin({
                        "process.env": {
                            // This has effect on the react lib size
                            NODE_ENV: JSON.stringify("production"),
                            BABEL_ENV: JSON.stringify('development/client')
                        }
                    }),
                    new webpack.optimize.DedupePlugin(),
                    new webpack.optimize.UglifyJsPlugin()
                )
            },
            "build-dev": {
                devtool: "sourcemap",
                debug: true
            }
        },
        "webpack-dev-server": {
            options: {
                webpack: webpackConfig,
                hot: true,
                publicPath: "/" + webpackConfig.output.publicPath
            },
            start: {
                keepAlive: true,
                hot: true,
                webpack: {
                    devtool: "eval",
                    debug: true
                }
            }
        },
        watch: {
            app: {
                files: ["src/**/*"],
                tasks: ["webpack:build-dev"],
                options: {
                    spawn: false
                }
            },

            jest: {
                files: ["src/**/*-test.js"],
                tasks: ["jest"],
                options: {
                    spawn: false
                }
            }
        },

        jest: {
            options: {
                coverage: true,
                testPathPattern: /.*-test.js/
            }
        },

        eslint: {
            target: ['src/**/*']
        }

    });

    grunt.registerTask("default", ["webpack-dev-server:start"]);

    grunt.registerTask("dev", ["webpack:build-dev", "watch:app"]);

    grunt.registerTask("build", ["webpack:build"]);

    grunt.registerTask("test", ["watch:jest"]);
};