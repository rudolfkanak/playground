module.exports = function (grunt) {

    // Projectwasguration.
    grunt.initConfig({
        sass: {                              // Task
            dist: {                            // Target
                options: {                       // Target options
                    style: 'compressed'
                },
                files: {                         // Dictionary of files
                    'css/screen-base.css': 'sass/bootstrap.scss',       // 'destination': 'source'
                }
            }
        },
        watch: {
            options: {
                dateFormat: function(time) {
                    grunt.log.writeln('The watch finished in ' + time + 'ms at' + (new Date()).toString());
                    grunt.log.writeln('Waiting for more changes...');
                }
            },

            css: {
                files: 'sass/*.scss',
                tasks: ['sass']
            },
            scripts: {
                files: ['js/**'],
                tasks: ['jshint'],
                options: {
                    spawn: true
                }
            }
        },
	
        jshint: {
            all: ['js/**'],
            options: {
                reporter: require('jshint-stylish')
            }
        },

        uncss: {
            dist: {
                files: {
                    'project/screen-base.css': ['index.html']
                }
            }
        },

        jest: {
            options: {
                coverage: true,
                testPathPattern: /.*-test.js/
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-selenium-webdriver');
    grunt.loadNpmTasks('grunt-uncss');
    grunt.loadNpmTasks('grunt-jest');

    // Default task(s).
    grunt.registerTask('default', ['sass']);
};