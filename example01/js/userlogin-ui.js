
(function ($, D, W) {
    $.widget('ui.userlogin',  {

        _create: function () {
            var that = this;

            $(this.element).submit(function() {
                that.loggedOn();
            });
        },

        _destroy: function () {},

        loggedOn: function () {
            this._trigger("loggedOn", null, { 'email': 'rudolfkanak@gmail.com', 'password': '123456789' });
            return this;
        }
    });
}(jQuery, window, document));
