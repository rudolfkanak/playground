var storageRepository = storageRepository || (function() {
        "use restrict";

        var _defaultOptions= function() {};
        var _storage = [];
        var _serviceApi = {};

        function _verifyServiceApi(serviceApi) {
            if (_.isEmpty(serviceApi) || _.isUndefined(serviceApi)) {
                throw new Error("Argument serviceApi is not defined.");
            }

          _serviceApi = serviceApi;
        }

        return {
            // public API

            /**
             * Add product
             *
             * @param {object} item - product
             * */
            add: function (item) {
                var dfd = jQuery.Deferred();
                console.info('Call method add');

                _storage.push(item);

                _serviceApi.save('products', _storage).then(function (data) {
                    dfd.resolve();
                });

                return dfd;
            },

            /**
             * Delete item of storage
             *
             * @param {int} index - The index of array
             */
            delete: function(index) {
                var dfd = jQuery.Deferred();
                console.info('Call method delete');

                _storage.splice(index);

                _serviceApi.save('products', _storage).then(function (data) {
                    dfd.resolve();
                });

                return dfd;
            },

            /**
             * Get all item of storage             *
             */
            getAll: function() {
                var dfd = jQuery.Deferred();
                console.info('Call get all products.');

                _serviceApi.get('products').then(function (data) {
                    if (_storage.length === 0) {
                        _storage = data;
                    }
                    
                    dfd.resolve(_storage);
                });

                return dfd;
            },

            getCountItems: function() {
                return _storage.length;
            },

            init: function(serviceApi, options) {
                $.extend(_defaultOptions, options);

                _verifyServiceApi(serviceApi);
            }
        };
    }());
