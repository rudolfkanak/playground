/*****
 *
 * users: [
 *   {
 *      id: '0',
 *      firstName: 'Rudolf',
 *      lastName: 'Kanak',
 *      email: 'rudolfkanak@gmail.com',
 *      password: '123456789',
 *      address: '',
 *      postCode: '',
 *      country: '',
 *      phone: '',
 *      abount: '',
  *     createDateTime: '',
  *     isLoggedOn: true,
  *     lastLogged: date
  *     }
 * ];
 *
 * **/

var users = users || (function() {

        var _defaultOptions= function() {};
        var _users = [];
        var _serviceApi = {};

        function _verifyServiceApi(serviceApi) {
            if (_.isEmpty(serviceApi) || _.isUndefined(serviceApi)) {
                throw new Error("Argument serviceApi is not defined.");
            }

            _serviceApi = serviceApi;
        }

        return {
            // Public API

            add: function (item) {
                var dfd = jQuery.Deferred();
                console.info('Call method add');

                _users.push(item);

                _serviceApi.save('users', _users).then(function (data) {
                    dfd.resolve();
                });

                return dfd;
            },

            delete: function(index) {
                var dfd = jQuery.Deferred();
                console.info('Call method delete');

                _users.splice(index);

                _serviceApi.save('users', _users).then(function (data) {
                    dfd.resolve();
                });

                return dfd;
            },

            getAll: function() {
                var dfd = jQuery.Deferred();
                console.info('Call get all products.');

                _serviceApi.get('users').then(function (data) {
                    if (_users.length === 0) {
                        _users = data;
                    }

                    dfd.resolve(_users);
                });

                return dfd;
            },
            /* Get total count */
            getCountItems: function() {
                return _users.length;
            },

            init: function(serviceApi, options) {
                $.extend(_defaultOptions, options);

                _verifyServiceApi(serviceApi);
            }
        };
    }());

module.exports = users;