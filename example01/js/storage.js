var storage = storage || (function() {
        "use restrict";

        // Private
        var table;
        var globalImagePath = './images';
        var product_items = [
            {image: globalImagePath + '/item.jpg', name: 'Dell PowerEdge', count: '0', price: 2000},
            {image: globalImagePath + '/item.jpg', name: 'Dell PowerEdge', count: '0', price: 2000},
            {image: globalImagePath + '/item.jpg', name: 'Dell PowerEdge', count: '0', price: 2000},
            {image: globalImagePath + '/item.jpg', name: 'Dell PowerEdge', count: '0', price: 2000},
            {image: globalImagePath + '/item.jpg', name: 'Dell PowerEdge', count: '0', price: 2000},
            {image: globalImagePath + '/item.jpg', name: 'Dell PowerEdge', count: '0', price: 2000},
            {image: globalImagePath + '/item.jpg', name: 'Dell PowerEdge', count: '0', price: 2000}
        ];

        var getEditRowTemplate = function (item) {
            return '<td>0</td>' +
                                '<td><input type="text" /></td>' +
                                '<td><input type="number" value="' + (item === null ? 0:item.count) + '" min="0" max="20"></td>' +
                                '<td><input type="number" value="' + (item === null ? 0:item.price) + '"></td>' +
                                '<td><input type="text" />0</td>' +
                                '<td><a href="#" title="Add new item" class="add"><i class="icon icon-add" title="Add new item"></i></a></td>';
        };

        var bindEventHandler = function(target, eventType, callMethod) {
            target.addEventListener(eventType, function() { console.log(this); });
            return target;
        };

        var insertCellToRowAndAddEvent = function (row, cellIndex, cellContent, events) {
            var cell = row.insertCell(cellIndex);
            cell.innerHTML = cellContent;

            if (events !== undefined) {
                cell.addEventListener(events.eventType, events.callMethod, false);
            }
        };

        var changePrice = function(e) {
            var actualCount = e.target.value;
           changeTotalPrice(e.target.parentNode.parentNode.childNodes[4], actualCount);
        };

        var changeTotalPrice = function(totalPriceElement, num) {
            totalPriceElement.innerHTML = num;
        };

        var addEventListenerByClass = function(className, event, fn) {
            var list = table.getElementsByClassName(className);
            for (var i = 0, len = list.length; i < len; i++) {
                list[i].addEventListener(event, fn, false);
                list[i].rowIndex = i;
            }
        };


        var getActualTableRowIndex = function(target) {
            return target.parentNode.parentNode.parentNode.getAttributeNode('data-row-index').value;
        };

        var removeTableRow = function (e) {
            if(confirm('Are you sure remove row?')) {
                table.deleteRow(getActualTableRowIndex(e.target));
            }
        };

        var changeTableRowToEditMode = function(e) {
            //'<input type="number" value="' + product_items[r].count + '" min="0" max="20">', { eventType: 'change', callMethod: changePrice});
        };

        var insertEmptyEditRow = function () {
            var editRow = table.insertRow(0);
            editRow.innerHTML = getEditRowTemplate(null);

            table.getElementsByClassName('add')[0].addEventListener('click', function() { alert('Insert'); });
        };

        return {
            // Public method

            init: function(tableElement, addElement) {
                if (tableElement === '' || tableElement === undefined &&
                    addElement === '' || addElement === undefined)
                    alert('Please fill table selector!');

                table = document.getElementById(tableElement).tBodies[0];
                document.getElementById(addElement).addEventListener('click', insertEmptyEditRow);
            },

            renderRows: function() {

                for (var r=0; r < product_items.length; r++) {
                    var row = table.insertRow(r);

                    var att = document.createAttribute("data-row-index");
                    att.value = r;
                    row.setAttributeNode(att);

                    insertCellToRowAndAddEvent(row, 0, '<img src="' + product_items[r].image + '" alt="Dell PowerEdge" width="50" height="50" />');
                    insertCellToRowAndAddEvent(row, 1, product_items[r].name + '_' + r);
                    insertCellToRowAndAddEvent(row, 2, product_items[r].count);
                    insertCellToRowAndAddEvent(row, 3, product_items[r].price);
                    insertCellToRowAndAddEvent(row, 4, 0);
                    insertCellToRowAndAddEvent(row, 5, '<a href="#" title="Edit item" class="edit"><i class="icon icon-edit" title="Edit item"></i></a> <a href="#" title="Remove item" class="remove"><i class="icon icon-remove" title="Remove item"></i></a>');
                }

                addEventListenerByClass('edit', 'click', changeTableRowToEditMode);
                addEventListenerByClass('remove', 'click', removeTableRow);
            }
        };
    }());