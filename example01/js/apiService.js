var apiService = apiService || (function() {
        "use restrict";
        //{ timestamp: '', token: '' }
        var validToken = null;

        var defaultOptions = {
            url: {
                tokenGenerateUrl: 'https://uuos9.plus4u.net/uu%3A0%3AARTIFACT%3FUUproto%3AOS%2FService%2FLogin%3AcallTokenGenerate',
                saveUrl: 'https://uuos9.plus4u.net/uu%3ADEV0135-BT%3AARTIFACT%3FUU%3ASTORAGE%3AStorage%2Fsave',
                loadUrl: 'https://uuos9.plus4u.net/uu%3ADEV0135-BT%3AARTIFACT%3FUU%3ASTORAGE%3AStorage%2Fload',
                deleteUrl: 'https://uuos9.plus4u.net/uu%3ADEV0135-BT%3AARTIFACT%3FUU%3ASTORAGE%3AStorage%2Fdelete'
            },
            header: {
                accept: 'application/json',
                UU_EAC: '6861666861663a6e61666e6166'
            },
            tokenRequestBody: '{"data":{"outputTo":"data"}}',
            method: 'POST',
            dataStorage: 'SERVER', //LOCAL OR SERVER,
            dataKey: "RK_DATA",
            tokenExpireTime: (30 * 60 * 1000) // 30min
        };

        var pushRequest = function (url, isToken, content) {
            var dfd = jQuery.Deferred();

            $.ajax(url, {
                type: defaultOptions.method,
                headers: isToken ? { "Accept": defaultOptions.header.accept, "UU_EAC": defaultOptions.header.UU_EAC }
                    : { "Accept": defaultOptions.header.accept, "UU_CT": validToken.token },
                data: content
            }).done(function (value) {
                dfd.resolve(value);
            }).error(function () {
                throw new Error("Server error.");
            });

            return dfd;
        };

        var requestToken = function() {
            var dfd = jQuery.Deferred();

                pushRequest(defaultOptions.url.tokenGenerateUrl, true, defaultOptions.tokenRequestBody).then(function (value) {
                    validToken = {
                        timestamp: new Date(new Date().getTime() + defaultOptions.tokenExpireTime).getTime(),
                        token: JSON.parse(value).data.UU_CT
                    };
                    saveTokenInfo(validToken);
                    dfd.resolve();
                });

            return dfd;
        };

        var saveTokenInfo = function(validToken) {
            localStorage.setItem('validTokenTimestamp', validToken.timestamp);
        };

        var isTokenValid = function () {
            return (validToken !== null && parseInt(localStorage.getItem('validTokenTimestamp')) >= new Date().getTime());
        };

        var buildSaveWrapperRequest = function(parameterName, dataContent) {
            var saveData = {};
            saveData["dataKey"] = defaultOptions.dataKey;
            saveData[parameterName] = dataContent;

            return JSON.stringify({"data": saveData});
        };

        var getDataByParameterName = function(data, parameterName) {
            var dataAsObj = JSON.parse(data);

            if (!_.isUndefined(dataAsObj.data[parameterName])) {
                return dataAsObj.data[parameterName];
            }

            throw new Error("Parameter name '" + parameterName + "' not found.");
        };

        return {
            // public API

            save: function (parameterName, dataContent) {
                var dfd = jQuery.Deferred();

                if (isTokenValid()) {
                    console.info('Save data with parameter name "' + parameterName + '"  and values "' + JSON.stringify(dataContent) + '"');
                    return pushRequest(defaultOptions.url.saveUrl, false, buildSaveWrapperRequest(parameterName, dataContent));
                } else {
                    requestToken().then(function() {
                       pushRequest(defaultOptions.url.saveUrl, false, buildSaveWrapperRequest(parameterName, dataContent)).then(function (value) {
                           console.info('Save data with parameter name "' + parameterName + '" and values "' + JSON.stringify(dataContent) + '"');
                           dfd.resolve(value);
                       });
                    });
                }

                return dfd;
            },

            delete: function(parameterName, dataContent) {
                return this.save(parameterName, dataContent);
            },

            get: function(parameterName) {
                var dfd = jQuery.Deferred();

                if (isTokenValid()) {
                    pushRequest(defaultOptions.url.loadUrl, false, '{"data":{"dataKey":"RK_DATA"}}').then(function (value) {
                        dfd.resolve(getDataByParameterName(value, parameterName));
                    });
                } else {
                    requestToken().then(function() {
                        pushRequest(defaultOptions.url.loadUrl, false, '{"data":{"dataKey":"RK_DATA"}}').then(function (value) {
                            dfd.resolve(getDataByParameterName(value, parameterName));
                        });
                    });
                }

                return dfd;
            },

            clearAllData: function () {
               var dfd = jQuery.Deferred();

                if (isTokenValid()) {
                    return pushRequest(defaultOptions.url.deleteUrl, false, '{"data":{"dataKey":"RK_DATA"}}');
                } else {
                    requestToken().then(function() {
                        pushRequest(defaultOptions.url.deleteUrl, false, '{"data":{"dataKey":"RK_DATA"}}').then(function (value) {
                            dfd.resolve(value);
                        });
                    });
                }

                return dfd;
            },

            init: function(options) {
                var self = this;
                $.extend(defaultOptions, options);

                return this;
            }
        };
    }());
