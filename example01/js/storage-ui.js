
(function ($, D, W) {
    $.widget('ui.storage',  {

        _globalImagePath: './images',

        _product_items: [
            {image: '{0}/item.jpg', name: 'Dell PowerEdge', count: '0', price: 2000},
            {image: '{0}/item.jpg', name: 'Dell PowerEdge', count: '0', price: 2000},
            {image: '{0}/item.jpg', name: 'Dell PowerEdge', count: '0', price: 2000},
            {image: '{0}/item.jpg', name: 'Dell PowerEdge', count: '0', price: 2000},
            {image: '{0}/item.jpg', name: 'Dell PowerEdge', count: '0', price: 2000},
            {image: '{0}/item.jpg', name: 'Dell PowerEdge', count: '0', price: 2000},
            {image: '{0}/item.jpg', name: 'Dell PowerEdge', count: '0', price: 2000}
        ],

        options: {
            globalImagePath: './images',
            rowTemplate: '<td><img src="{0}" alt="Dell PowerEdge" width="50" height="50" /></td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td><td>{4}</td><td><a href="#" title="Edit item" class="edit"><i class="icon icon-edit" title="Edit item"></i></a> <a href="#" title="Remove item" class="remove"><i class="icon icon-remove" title="Remove item"></i></a></td>'
        },

        $table: null,

        $removeConfirmDialog: null,

        _create: function () {
            var that = this;

            this._initRemoveConfirmDialog();
            this._setTable();
            this._renderTableRows(this.$table, this.options);
        },

        _initRemoveConfirmDialog: function() {
            this.$removeConfirmDialog =  $('#remove-dialog-confirm').dialog({
                autoOpen: false,
                resizable: false,
                height:140,
                modal: true,
                buttons: {
                    "Delete all items": function() {
                        $(this).dialog( "close" );
                    },
                    Cancel: function() {
                        $(this).dialog( "close" );
                    }
                }
            });
        },

        _setTable: function() {
            this.$table = $(this.element).find('table');
        },

        _renderRow: function(index, value, isPrepend) {
            var fullImagePath = value.image.replace('{0}', this.options.globalImagePath);
            var row = $('<tr></tr>')
                .append('<td><img src="' + fullImagePath + '" alt="Dell PowerEdge" width="50" height="50" /></td>')
                .append('<td>' + value.name + '</td>')
                .append('<td>' + value.count + '</td>')
                .append('<td>' + value.price + '</td>')
                .append('<td><a href="#" title="Edit item" class="edit" data-row-index="' + index + '"><i class="icon icon-edit" title="Edit item"></i></a> <a href="#" title="Remove item" data-row-index="' + index + '" class="remove"><i class="icon icon-remove" title="Remove item"></i></a></td>');

            if (isPrepend) {
                this.$table.find('tbody tr:first').after(row);
            } else {
                this.$table.find('tbody').append(row);
            }
        },

        _renderTableRows: function ($table, options) {
            var self = this;
            self.options.storageRepo.getAll('products').then(function (products) {
                $.each(products, function( index, value ) {
                    self._renderRow(index, value, false);
                });

                self._createEmptyEditTableRow();

                // Binding event for edit/remove
                self.$table.on('click', 'a.edit', function() { alert('sadsa'); });
                self.$table.on('click', 'a.remove', { removeConfirmDialog: self.$removeConfirmDialog, storageRepo: self.options.storageRepo }, self._removeRow);
            });
        },

        _removeRow: function (e) {
            var self = this;

            e.data.removeConfirmDialog.dialog({
                buttons: {
                    "Delete": function() {
                        var that = this;

                        e.data.storageRepo.delete($(self).attr('data-row-index')).then(function (data) {
                            $(that).dialog( "close" );
                            $(self).parents('tr').hide();
                        });
                    },
                    Cancel: function() {
                        $(this).dialog( "close" );
                    }
            }});
            
            e.data.removeConfirmDialog.dialog('open');
        },

        _createEmptyEditTableRow: function () {
            var self = this;

            var emptyEditRow = "<tr>" +
                "<td></td>" +
                "<td><input type='text' name='name' /></td>" +
                "<td><input type='number' name='count' /></td>" +
                "<td><input type='number' name='price' /></td>" +
                "<td><a href=\"#\" title=\"Add new item\" id=\"addNewProduct\"><i class=\"icon icon-add\">&nbsp;</i></a></td>" +
                "</tr>";

            this.$table.find('tbody').prepend(emptyEditRow);

            $('#addNewProduct').on('click', function () {

                var saveProduct = {};
                saveProduct.image = '{0}/item.jpg';

                self.$table.find('tbody :input').each(function(){
                    var name = $(this).attr('name');
                    var value = $(this).val();

                    saveProduct[name] = value;
                });

                self.options.storageRepo.add(saveProduct).then(function () {
                    // cleaning
                    self.$table.find('tbody :input').each(function(){
                        $(this).val('');
                    });

                    self._renderRow(self.options.storageRepo.getCountItems() - 1, saveProduct, true);
                });
            });
        },

        _destroy: function () {}
    });
}(jQuery, window, document));
