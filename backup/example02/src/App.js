import React, {Component, PropTypes} from 'react';
import Comment from './Comment';
import * as CommentActions from '../actions'

var COMMENTS = [];

export default class App extends Component {
   render() {
        return (
            <div className="container">
                <h1>Comments</h1>
                <Comment comments={COMMENTS} />
            </div>
        );
    }
}