import React, {Component} from 'react';
import CommentAdd from './CommentAdd';
import CommentList from './CommentList';
import { connect } from 'react-redux';
import * as commentsActions from '../redux/comments'

class Comment extends Component {
  
  constructor(props, context) {
    super(props, context);
    this.state = {
      data: []
    };
  }

  handleCommentSubmit (postText) {
    let { addComment } = this.props;
    
    addComment({name: 'Rudolf Kanak', text: postText})
  }

  handleRemovecomment (e, itemId) {
    let { removeComment } = this.props;
    
    removeComment(itemId)
  }

  componentDidMount () {
    let { reloadComments } = this.props;

    setTimeout(function() {
      reloadComments([{id: 0, name: 'Rudolf Kanak', text: 'hello world'}]);
    }.bind(this), 1000);
  }

  render() {
    const { comments, actions } = this.props
    return (
      <div className="container-form">
        <CommentList comments={this.state.data} onClick={ (e, itemId) => this.handleRemovecomment(e, itemId) } />
        <CommentAdd onSubmit={(e) => this.handleCommentSubmit(e)} />
      </div>
    );
  }
}

export default connect(state => state, commentsActions)(Comment)


