import React, {Component, PropTypes} from 'react';

export default class CommentAdd extends Component {

    static propTypes = {
        textPost: PropTypes.number
    };

    handleChange(e) {
        e.preventDefault();

        this.props.onSubmit(
            this.refs.postInput.value
        );

        this.refs.postInput.value = '';
    }

    render() {
        return (
            <div className="comment-form">
                <form onSubmit={(e) => this.handleChange(e)}>
                    <input type="text" ref="postInput" value={this.props.textPost} placeholder="Post comment.." />
                    <input type="submit" value="Post" />
                </form>
            </div>
        );
    }
}

