//import { ADD_COMMENT, RELOAD_COMMENTS, REMOVE_COMMENTS } from '../actions/index'

const ADD_COMMENT = 'ADD_COMMENT';
const REMOVE_COMMENT = 'REMOVE_COMMENT';
const RELOAD_COMMENTS = 'RELOAD_COMMENTS';

const initialState = [];

export default function comments(state = initialState, action) {
    switch (action.type) {
        case ADD_COMMENT:
            var data = [
                ...state,
                {
                    id: state.reduce((maxId, todo) => Math.max(todo.id, maxId), -1) + 1,
                    text: action.item.text,
                    name: action.item.name
                }                
            ];
            return data;
        case REMOVE_COMMENT:
            var newData = state.slice(); 
            newData.splice(action.itemId, 1); 
            return newData;
        case RELOAD_COMMENTS:            
            return action.items;
        default:
            return state
    }
}

export function addComment(item) {
  return { type: ADD_COMMENT, item };
}

export function removeComment(itemId) {    
  return { type: REMOVE_COMMENT, itemId };
}

export function reloadComments(items) {
  return { type: RELOAD_COMMENTS, items };
}