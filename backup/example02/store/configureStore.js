import { createStore } from 'redux'
import rootReducer from '../redux'

function configureStore(initialState) {
  return createStore(rootReducer, initialState);
}

module.exports = configureStore;